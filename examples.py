from functools import partial
from menu import Menu
class fibonacci_menu:
    _list = list

    def __init__(self):
        _menu = {"reset":self.reset,
                "list":self.list,
                "back":lambda:Menu.BACK}
        self.menu = Menu(_menu, docs="A fibonacci number generator.", suppress_implied=True, dwrap=False)
        self.reset()

    @staticmethod
    def fibonacci():
        a = 0
        b = 1
        while True:
            yield b
            b = b + a
            a = b - a
      
    def reset(self):
        """Start over from 1."""
        self.current_generator = self.fibonacci()

    def list(self, x=1) -> _list:
        """List x fibonacci numbers."""
        return [next(self.current_generator) for _ in range(int(x))]

if __name__ == "__main__":
    fibonacci = fibonacci_menu()


    menu = Menu(fibonacci = fibonacci.menu, docs = "Here, see examples of Menu usage!")

    menu.loop()