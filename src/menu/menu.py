"""Usable command-line interface system with asynchronous tendencies

>>> from menu import Menu

##Basic Usage##
Menu drops the user into an input loop when its `Menu.loop()` function is 
called -- note that display output is excluded from these docstrings for
simplicity:

>>> a = Menu()
>>> a.loop()
... Enter Command: 

When the user types a command from the input loop, the object it is mapped to
by the active Menu will be called with any arguments provided, and the return
value will be printed. If the object is a Menu, that Menu's loop called.
Using Menu's loop, if the object is a coroutine function, TypeError is raised.

Menu has built-in generated help functionality (accessible to the user by
typing 'help' at the prompt) and some basic output formatting by default,
see the Menu class's docs.

Menus are created from dicts, and can be treated like dicts:
>>> a = Menu({'one':lambda:1})
>>> a['two'] = lambda:2

This allows you to create complicated menu trees with relative ease, as in the
following fictional example -- each dict is converted to its own Menu:
>>> data = {"search":DataAPI.search,
...         "reports":{"new":DataAPI.get_new_entries,
...                    "expired":DataAPI.get_expired_entries,
...                    "pending":partial(DataAPI.get_pending, number=10),},
...         "read_file":DataAPI.import_file,
...         "settings":{"database":{"set_source":DataHandler.set_source,
...                                 "username":DataHandler.set_user,},
...                     "output":DataHandler.config_output,
...                     "display":{"screen_width":set_width,
...                                "colors":config_colors,},},}
>>> menu = Menu(data)

##Subclassing##
Menu can be subclassed, and different subclasses of Menu can be cast between
each other with ease. To subclass a menu, include an `init_params` argument,
listing the novel parameters of the subclass's `__init__`:

>>> class AMenu(Menu, init_params = ["_aprint", "_ainput"]):
>>>     def __init__(self, data=None, *, _aprint, _ainput, **kwargs):
...         # do something with _aprint and _ainput
...         super().__init__(data=data, _aprint=_aprint, _ainput=_ainput, **kwargs)

Note that passing all parameters including variable kwargs to super()'s init
is necessary to facilitate casting.

###`AMenu`###
AMenu is a subclass provided by the menu package; the only difference between Menu
and AMenu is that `AMenu.loop()` is a coroutine function, and can handle coroutine
functions mapped to its commands.

>>> b = AMenu()
>>> await a.loop()
... Enter Command: 

☭ Communist Content
"""
import copy
import io
import sys
import asyncio
import inspect
from collections import UserDict, namedtuple
from collections.abc import Mapping
from functools import partial
from itertools import starmap
from typing import Callable, Union
from inspect import signature
import pprint

from dicttools import factory_replace, DictHeap
from dicttools.util import binarytree as bt

# __all__ = ["Menu",
#            "AMenu",
#            "ainput",
#            "aprint"]

# Helper classes for Menu
# Fully tested
class Command:
    """A data class for commands represented in the Menu"""

    def __init__(self, command, func):
        """command represents the Menu key; func the value

        useful pattern: starmap(Menu.Command, menu.items())
        """
        self.command = command
        self.menu = isinstance(func, Menu)
        self.signature = signature(
            func) if not self.menu else signature(lambda: None)
        self.docs = inspect.getdoc(func)
        if self.docs is None:
            self.docs = "NO DOCUMENTATION"

    # For use in doc output/interface
    @staticmethod
    def describe_signature(signature: inspect.Signature) -> str:
        """Function signature in a Menu-input-compatible format.

        Square brackets [] indicate a default or otherwise-optional parameter.
        Angled brackets <> indicate the default value.
        Parentheses () indicate a type annotation.

        Annotation is inferred if default is present, and annotation isn't.

        >>> def signature1(p_only_d: int = 2, /, p_or_k_d = 3, *, k_only_d: int = 4): ...
        >>> def signature2(p_only: int, /, p_or_k: int, *var_p, k_only, k_only2: int, **var_k): ...
        >>> describe_signature(inspect.signature(signature1))
        ... "[p_only_d (int) <2>]   [p_or_k_d (int) <3>]   [k_only_d (int) = <4>]"
        >>> describe_signature(inspect.signature(signature2))
        ... "p_only (int)   p_or_k (int)   [p1 [p2] [...]]   k_only (...) =   k_only2 (int) =   [p1 = v1 [p2 = v2] [...]]"
        """
        out = list()
        for param in signature.parameters.values():
            default = f"<{repr(param.default)}>" if param.default is not inspect.Parameter.empty else ""
            if param.annotation is inspect.Parameter.empty:
                if default == "":
                    annotation = "(...)"
                else:
                    annotation = f"({type(param.default).__name__})"
            else:
                if isinstance(param.annotation, type):
                    annotation = f"({param.annotation.__name__})"
                else:
                    annotation = f"({param.annotation})"
            _repr = f"{param.name}"

            match param.kind:
                case inspect.Parameter.VAR_POSITIONAL:
                    _repr = "[p1 [p2] [...]]"
                case inspect.Parameter.VAR_KEYWORD:
                    _repr = "[k1 = v1 [k2 = v2] [...]]"
                case inspect.Parameter.KEYWORD_ONLY:
                    # "[kw_arg (annotation) ="
                    _repr = f"[{param.name} {annotation} ="
                    if default:
                        # "[kw_arg (annotation) = <default>]"
                        _repr += f" {default}]"
                    else:
                        # "k_arg (annotation) ="
                        _repr = _repr[1:]
                # POSITIONAL_ONLY or POSITIONAL_OR_KEYWORD
                case _:
                    # "[pos_arg "
                    _repr = f"[{param.name} {annotation}"
                    if default:
                        # "[pos_arg (annotation) <default>]"
                        _repr += f" {default}]"
                    else:
                        # "pos_arg (annotation)"
                        _repr = _repr[1:]
            out.append(_repr)

        return "   ".join(out)

    def help(self):
        """Help block for the referenced command in a tuple"""
        # To be formatted by the Menu
        cmd_sig = f"{self.command} {self.describe_signature(self.signature)}"
        body = "\n".join([cmd_sig, "", self.docs])

        return body

    def short_help(self):
        """Short help for the referenced command"""
        cmd_sig = f"{self.command} {self.describe_signature(self.signature)}"
        doc = self.docs.split("\n")[0]
        out = "\n\t".join([cmd_sig, doc])

        return out


class Menu(UserDict):
    """The `Menu` class is a dictionary mapping strings to callables and Menus.

    `Menu` can be instantiated with mappings, provided their keys and values
    are strings and callables, menus, or other mappings. However, `Menu` raises
    TypeError when non-string keys, or non-callable-or-Menu values, including
    mappings, are added to an instance after its creation.

    When Menu processes a dict on creation, it calls `Menu.menu_factory()` on
    `data` to change it to a dict if it isn't one already; then that dict
    becomes the Menu. All detected Menus in the `data` dict will be cast to the
    appropriate Menu type, preserving parametric settings where possible. All
    other detected mappings will be converted to Menus, propagating the passed
    parameters.

    For example:
    >>> a = AMenu({"one":Menu(), "two":{"one":print}},
    ...           prompt="prompt")
    >>> list(map(lambda o:isinstance(o, AMenu),
    ...          [a, a["one"], a["two"]]))
    ... [True, True, True]
    >>> a._prompt == a["two"]._prompt == "prompt"
    ... True
    >>> a["one"]._prompt == "prompt" # originally a Menu() with its own parameters
    ... False

    Callables should be able to handle string inputs for each argument, and
    return outputs with a sane `__str__` representation (so print is a bad
    example). Menu's built-in help functions will use the callable's docstring
    and signature including type annotations to present useful information to
    the user on how to interact with the callable through the menu.

    Most of Menu's output is displayed with a generated header and footer line.
    This behavior can be roughly controlled with the `Menu.S_WIDTH`, `Menu.L_CHAR`, and
    `Menu.LINE_SEP` class members.

    `Menu.S_WIDTH` is screen width, default `78`.
    `Menu.L_CHAR` is the horizontal line-drawing character; default `=`.
    `Menu.LINE_SEP` is the default line seperator; default `\\n`.

    This formatting can be disabled entirely by setting class
    member `Menu.DISPLAY_WRAP` to False.

    All Menus have three built-in commands:
    back        drops out of the Menu's input loop.
    help        displays help on commands, and the Menu's docs.
    structure   displays the Menu structure from the current Menu
        down through submenus."""
    BACK = object()
    """A special value returned by command functions to exit the menu loop"""
    _BACK_CMD = "back"
    _HELP_CMD = "help"
    _STRUCTURE_CMD = "structure"

    S_WIDTH = 78
    """Width for `Menu.display_wrap`"""
    L_CHAR = "="
    """Line character for `Menu.display_wrap`"""
    LINE_SEP = "\n"
    """Line seperator for `Menu.display_wrap`"""
    DISPLAY_WRAP = True
    """Controls `Menu.display_wrap`ping"""

    @property
    def display(self):
        return self.dwrap("", self._display if self._display is not None else self.help())

    @property
    def prompt(self):
        return self._prompt

    _class_init_params = ["display",
                          "prompt",
                          "docs",
                          "suppress_implied",
                          "dwrap",]

    # Fully tested
    def __init__(self,
                 data: dict = None,
                 *,
                 display: str = None,
                 prompt: str = "Enter command: ",
                 docs: str = "A menu",
                 suppress_implied: bool = False,
                 dwrap: bool = True,
                 _top_level: bool = True,
                 **kwargs) -> None:
        """##Parameters##
        `data` can be a mapping of strings to callables, including a `Menu`.
        If more mappings are found embedded in the data, they will also be
        converted to Menus. Like dict, `Menu` will update itself with the keyword 
        arguments passed to its constructor.

        Example:
        >>> data = {"test":lambda:print("test!"), "test2":Menu()}
        >>> a = Menu(data, test3=lambda:print(3), dwrap = False)
        >>> a.loop()
        ... Enter Command: test
        ... test!
        ... Enter Command: test2
        ... Enter Command: structure

        `display` is a string which is displayed before the prompt. If none is
        provided, it defaults to a list of avilable commands.

        `prompt` is the prompt for input. This is set per-menu -- if the user
        navigates to a sub-menu, the sub-menu's prompt will be used;
        "Enter Command: " by default.

        `docs` is a docstring displayed by the built-in help command; "A menu" by default.

        `suppress_implied` suppresses the built-in commands; False by dfeault.

        `dwrap` sets the Menu's `Menu.DISPLAY_WRAP` member; True by default.

        keyword arguments can be also used to override built-in commands without
        subclassing or altering the class directly. Overriding "back" isn't recommended.

        If you set `suppress_implied`, or otherwise override the "back" command, you should
        implement a command which returns `Menu.BACK` in order to break the input loop.        
        """
        self._instance_init_args = dict()
        for param in self._class_init_params:
            if param in kwargs:
                self._instance_init_args.update({param: kwargs.pop(param)})
            else:
                # defaults
                self._instance_init_args.update({param: locals()[param]})

        # Discard unusable init params (those still living in kwargs after
        # the previous loop). This will only happen if we're casting from a
        # subclass to a superclass of Menu.
        if isinstance(data, Menu):
            for param in data._class_init_params:
                if param in kwargs:
                    kwargs.pop(param)

        self._display = display
        self._prompt = prompt
        self.__doc__ = docs
        self.DISPLAY_WRAP = dwrap
        self.SUPPRESSED_IMPLIED = suppress_implied
        if data is None:
            data = dict()
        data.update(kwargs)
        # Only Menus created by the user will bother creating dictheaps and checking Mappings
        if _top_level:
            self.dictheap = DictHeap(data, include_data=True)
            all_mappings_instance_this = all(isinstance(self.dictheap.get_data(i), type(self))
                                            or not isinstance(self.dictheap.get_data(i), Mapping)
                                            for i in self.dictheap.topological_order[:-1])
            if not all_mappings_instance_this:
                data = factory_replace(data, partial(type(self), _top_level = False, **self._instance_init_args))
                self.dictheap = DictHeap(data, include_data=True)
        else:
            self.dictheap = None

        super().__init__(data)

        if not suppress_implied:
            self.update({Menu._HELP_CMD: self.help,
                         Menu._STRUCTURE_CMD: self.structure,
                         Menu._BACK_CMD: self.back, })

    def __init_subclass__(cls, init_params=[]):
        cls._class_init_params = copy.copy(cls._class_init_params)
        cls._class_init_params.extend(init_params)

    # Called implicitly by super().__init__, .update, .setdefault
    # No need for extra validation in override and watch out for loops
    def __setitem__(self, key: str, item) -> None:
        if (self._validate_key(key) and
                self._validate_value(item)):
            return super().__setitem__(key, item)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({super().__repr__()})"

    @staticmethod
    def _parse_input(input: str, **kwargs: dict):
        class _ParseInputResult(namedtuple('ParsedInputResultBase', ["command", "args", "kwargs"])):
            ...
        """Separate input into a command, args, and kwargs.

        Right now, this only passes kwargs through which were passed to it."""
        input = input.strip().split()

        return _ParseInputResult(command=input[0], args=tuple(input[1:]), kwargs=kwargs)

    @staticmethod
    def _validate_value(value, false_err=True):
        """Raises TypeError if Value is invalid; otherwise returns True"""
        if callable(value) or isinstance(value, Menu):
            return True
        else:
            if false_err:
                raise TypeError("Item is invalid callback")
            else:
                return False

    @staticmethod
    def _valid_value_or_mapping(i, false_err=True):
        """Raises TypeError if value is invalid and not a mapping; otherwise returns True"""
        if Menu._validate_value(i, false_err=False) or isinstance(i, Mapping):
            return True
        else:
            if false_err:
                raise TypeError(
                    "Item is neither a valid callback nor a mapping.")
            else:
                return False

    @staticmethod
    def _validate_key(key, false_err=True):
        """Raises TypeError if Key is invalid; otherwise returns True"""
        if isinstance(key, str):
            return True
        else:
            if false_err:
                raise TypeError("Key is invalid string")
            else:
                return False

    # Fully tested
    @staticmethod
    def _validate_dict(mapping: Mapping) -> Mapping:
        """Raises TypeError if mapping is invalid; otherwise returns the input."""
        validate_key = partial(Menu._validate_key, false_err=False)
        validate_value = partial(Menu._validate_value, false_err=False)
        valid_value_or_mapping = partial(
            Menu._valid_value_or_mapping, false_err=False)

        def should_process(v): return isinstance(
            v, Mapping) and not isinstance(v, Menu)

        if not all(map(validate_key, mapping.keys())):
            raise TypeError("Invalid key -- must be string")
        if not all(map(valid_value_or_mapping, mapping.values())):
            raise TypeError("Invalid value -- must be callable or mapping")
        # If there are no mappings embedded, just return the mapping.
        if all(map(validate_value, mapping.values())):
            return mapping
        # Otherwise, process non-Menu mappings recursively.
        for v in mapping.values():
            if should_process(v):
                Menu._validate_dict(v)

        return mapping

    @staticmethod
    def display_wrap(title: str, contents: str, s_width: int = 78, l_char: str = "=", line_sep: str = "\n", end: str = "\n") -> str:
        """Wrap contents in an appropriate header and footer"""
        h = "" if len(title) == 0 else f" {title} "
        header = f"{h:{l_char}^{s_width}}"
        bottom_line = l_char*s_width
        out = line_sep.join([header, str(contents), bottom_line]) + end

        return out

    # Fully tested
    def _as_dict(self):
        out = dict(self)
        if not self.SUPPRESSED_IMPLIED:
            del out[self._BACK_CMD]
            del out[self._STRUCTURE_CMD]
            del out[self._HELP_CMD]

        return out

    def dwrap(self, title, contents):
        """Treat contents according to `self.DISPLAY_WRAP`"""
        if not self.DISPLAY_WRAP:
            return contents
        return Menu.display_wrap(title, contents, s_width=self.S_WIDTH, l_char=self.L_CHAR, line_sep=self.LINE_SEP)

    def loop(self, input_string: str = "", *, _in_stream = sys.stdin, _out_stream=sys.stdout, **kwargs) -> None:
        """`instr` represents initial input, eg to force the user into a particular sub-menu.

        Keyword arguments are passed to the command, and down through the Menu chain."""

        minput = partial(sinput, in_stream=_in_stream, out_stream=_out_stream)
        mprint = partial(print, file=_out_stream, end="")

        while True:
            result = None
            # TODO: Cross-platform checks (this works on Windows 11)
            # Empty string means user hit enter. \n means prompt without display.
            if input_string == "":
                mprint(self.display)
            if input_string == "\n" or input_string == "":
                input_string = minput(self.prompt)
                continue
            parsed_input = self._parse_input(input_string, **kwargs)
            if parsed_input.command not in self:
                input_string = minput(f"Invalid command.\n{self.prompt}")
                continue
            command = self[parsed_input.command]
            try:
                if isinstance(command, Menu):
                    command.loop(*parsed_input.args, **parsed_input.kwargs, _in_stream=_in_stream, _out_stream=_out_stream)
                    # Set us up for menu display on return from sub-menu
                    input_string = ""
                    continue
                else:
                    result = command(*parsed_input.args, **parsed_input.kwargs)
            except TypeError:
                input_string = minput(f"Invalid arguments.\n{self.prompt}")
                continue

            # Interpret command result
            if asyncio.iscoroutine(result):
                raise TypeError("Cannot handle coroutine functions; use aloop")
            if result is Menu.BACK:
                break
            if result is not None:
                mprint(self.dwrap(parsed_input.command, result))
            # Don't display the menu here, to avoid smothering command output.
            input_string = "\n"
            continue
        return None

    # Functions designed for inclusion as Menu commands
    # docstring for these functions are designed to be seen by the user
    def help(self, *args):
        """Display help; with arguments, display command-specific help. eg 'help help'

        Commands may be entered by the user in the following formats:
        >>> Enter Command: 
        ... # To call a command with no arguments:
        ... command
        ... # To call a command, specifying its first two positional arguments:
        ... command 25 steve
        ... # To call a command, specifying those and a keyword argument:
        ... command 25 steve brusselssprout=delicious

        In help output, commands are displayed as the command, and arguments:
        >>> command arg1 [optional arg <default>] [optional keword arg = <default>]

        Arguments can be positonal or keyword (p or k).

        If nested optional arguments are listed, this indicates that any number of arguments
        can be entered, and the command will decide what to do with them.

        Conventions:
        Square brackets [] indicate an optional argument. Optional arguments are listed with their
        default value. Angled brackets <> indicate a default value..
        For non-optional arguments, if available, the argument's type will be displayed.
        eg int for integer, str for string. Parentheses () indicate an argument's type.

        Examples:
        >>> command [p_only_d (int) <2>]   [p_or_k_d (int) <3>]   [k_only_d (int) = <4>]
        >>> command2 p_only (int)   p_or_k (int)   [p1 [p2] [...]]   k_only (...) =   k_only2 (int) =   [k1 = v1 [k2 = v2] [...]]
        """
        out = []

        if len(args) == 0:
            commands = starmap(Command, self.items())
            out.extend([self.__doc__, "\n"])
            for cmd in commands:
                out.append(cmd.short_help())
        elif args[0] in self:
            if not isinstance(self[args[0]], Menu):
                cmd_help = Command(args[0], self[args[0]]).help()
                return cmd_help
            elif len(args) == 1:
                # Call Menus' own help() directly
                return self[args[0]].help()
            else:
                return self[args[0]].help(*(args[1:]))
        else:
            out = "No such command."

        return "\n".join(out)

    def back(self):
        """Leave this menu."""
        return Menu.BACK

    # Fully tested
    def structure(self, pretty: bool = True) -> Union[str | dict]:
        """Dict or pretty string of all commands in this and all sub-menus.

        Setting `pretty` to False will cause structure to return a dict.
        Otherwise, it returns a string.

        Excludes the implied "back", "help", and "structure" commands."""
        if pretty:
            out = ["root\n"]
            for node in bt.inorder_nodes(self.dictheap):
                if not self.SUPPRESSED_IMPLIED:
                    if self.dictheap.get_real_key(node) in ["back", "help", "structure"]:
                        continue
                out.append("    "*(self.dictheap.get_level(node)))
                out.append(f"- {self.dictheap.get_real_key(node)}\n")
            return "".join(out)
        
        all_commands = dict()
        all_commands['menu'] = list(self.keys())
        if not self.SUPPRESSED_IMPLIED:
            all_commands['menu'].remove("back")
            all_commands['menu'].remove("help")
            all_commands['menu'].remove("structure")
        for k, v in self.items():
            if isinstance(v, Menu):
                all_commands[k] = v.structure(pretty=False)

        return all_commands


class AMenu(Menu, init_params=["_aprint", "_ainput"]):
    """An asynchronous subclass of `Menu`."""

    def __init__(self,
                 data: dict = None,
                 *,
                 _aprint: Callable = None,
                 _ainput: Callable = None,
                 **kwargs):
        """##Parameters##

        `_ainput` sets the asynchronous input function, menu.ainput by default. Passed coroutine
        functions should accept a prompt and two keyword arguments: `_in_stream` and `_out_stream`.
        These should set the input and output streams for `ainput`.

        `_aprint` sets the asynchronous print function, `aprint` by default. Passed coroutine
        functions should replicate builtin `print` functionality."""
        self.ainput = _ainput if _ainput is not None else ainput
        self.aprint = _aprint if _aprint is not None else aprint

        super().__init__(data, _ainput=_ainput, _aprint=_aprint, **kwargs)
        # super().__init__(data, **kwargs)

    # Fully tested
    async def loop(self, instr: str = "", _in_stream=sys.stdin, _out_stream=sys.stdout, **kwargs) -> None:
        """Asynchronous version of `Menu.loop`, allowing for coroutine functions in the Menu

        This version of the loop also allows for specifying the input and output streams.

        _in_stream is the stream to read for input

        _out_stream is the stream to write to for output

        _in_stream, _out_stream, and other keyword arguments are passed to the command,
        and down through the Menu chain."""
        streams = {"_in_stream": _in_stream, "_out_stream": _out_stream}
        menu_kwargs = dict()
        menu_kwargs.update(streams)
        mprint = partial(self.aprint, file=_out_stream, end="")
        mainput = partial(self.ainput, **streams)

        while True:
            result = None
            if instr == "":
                await mprint(self.display)
            if instr == "\n" or instr == "":
                instr = await mainput(self.prompt)
                continue

            split = AMenu._parse_input(instr, **kwargs)
            if split.command not in self:
                instr = await mainput(f"Invalid command.\n{self.prompt}")
                continue

            command = self[split.command]
            try:
                if isinstance(command, AMenu):
                    split.kwargs.update(menu_kwargs)
                    await command.loop(*split.args, **split.kwargs)
                    instr = ""
                    continue
                else:
                    result = command(*split.args, **split.kwargs)
            except TypeError:
                instr = await mainput(f"Invalid arguments.\n{self.prompt}")
                continue

            if asyncio.iscoroutine(result):
                result = await result

            if result == AMenu.BACK:
                break

            if result is not None:
                await mprint(self.dwrap(split.command, result))
            instr = "\n"
            continue
        return None


# Asynchronous input() and print() functions inspired by https://stackoverflow.com/a/65326191
# Fully tested
async def ainput(prompt: str = "", _in_stream: io.TextIOBase = sys.stdin, _out_stream: io.TextIOBase = sys.stdout) -> str:
    """Asynchronous input()."""
    await aprint(prompt, file=_out_stream, end="", flush=True)

    output = await asyncio.get_event_loop().run_in_executor(
        None, _in_stream.readline)

    return output.strip()


# Fully tested
async def aprint(*values, sep=" ", end="\n", file=sys.stdout, flush=False):
    """Asynchronous print()."""
    values = sep.join(map(str, values))
    values = values + end

    await asyncio.get_event_loop().run_in_executor(
        None, lambda s=values: file.write(s)
    )

    if flush:
        file.flush()

def sinput(prompt: str = "", in_stream: io.TextIOBase = sys.stdin, out_stream: io.TextIOBase = sys.stdout) -> str:
    """input() with stream specifiers"""
    print(prompt, file=out_stream, end="", flush=True)

    output = in_stream.readline()

    return output.strip()