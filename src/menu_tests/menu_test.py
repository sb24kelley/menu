from collections import UserDict
from inspect import signature
import inspect
import unittest, io
from math import hypot
import menu as _menu

class menu_async_util(unittest.IsolatedAsyncioTestCase):
    async def test_ainput(self):
        """ainput functions as asynchronous input"""
        test_input = "Test Input\n"
        test_prompt = "Test prompt: "
        spoofio_input = io.StringIO(test_input)
        spoofio_output = io.StringIO()

        result = await _menu.ainput(test_prompt, _in_stream=spoofio_input, _out_stream=spoofio_output)
        spoofio_output.seek(0)
        self.assertEqual(spoofio_output.readline(), test_prompt)
        self.assertEqual(result, test_input[:-1])
    
    async def test_aprint(self):
        """aprint matches print functionality"""
        test_str = "test"
        test_list = ["test1", "test2"]
        spoof_output = io.StringIO()

        await _menu.aprint(test_str, file=spoof_output)
        await _menu.aprint(*test_list, file=spoof_output)
        await _menu.aprint(*test_list, sep="|", file=spoof_output)
        await _menu.aprint(test_str, end="!!!", file=spoof_output)
        await _menu.aprint(*test_list, sep="|", end="!!!", file=spoof_output)
        await _menu.aprint(25, file=spoof_output)

        expected = "test\ntest1 test2\ntest1|test2\ntest!!!test1|test2!!!25\n"

        spoof_output.seek(0)
        self.assertEqual(spoof_output.read(), expected)

class menu_util(unittest.TestCase):
    def test_sinput(self):
        """sinput functions as input"""
        test_input = "Test Input\n"
        test_prompt = "Test prompt: "
        spoofio_input = io.StringIO(test_input)
        spoofio_output = io.StringIO()

        result = _menu.sinput(test_prompt, in_stream=spoofio_input, out_stream=spoofio_output)
        spoofio_output.seek(0)
        self.assertEqual(spoofio_output.readline(), test_prompt)
        self.assertEqual(result, test_input[:-1])


class Command_class(unittest.TestCase):
    def test_describe_signature(self):
        """Command.describe_signature returns a correct string"""
        test_func = _menu.Command.describe_signature

        def signature1(p_only_d: int = 2, /, p_or_k_d = 3, *, k_only_d: int = 4): ...
        def signature2(p_only: int, /, p_or_k: int, *var_p, k_only, k_only2: int, **var_k): ...

        test1 = test_func(signature(signature1))
        test2 = test_func(signature(signature2))

        expected_output1 = "[p_only_d (int) <2>]   [p_or_k_d (int) <3>]   [k_only_d (int) = <4>]"
        expected_output2 = "p_only (int)   p_or_k (int)   [p1 [p2] [...]]   k_only (...) =   k_only2 (int) =   [k1 = v1 [k2 = v2] [...]]"

        self.assertEqual(test1, expected_output1)
        self.assertEqual(test2, expected_output2)
    
    def test_help(self):
        """Command.help() returns a string"""
        self.assertIsInstance(_menu.Command("test",print).help(), str)
    
    def test_short_help(self):
        """Command.short_help() returns a string"""
        self.assertIsInstance(_menu.Command("test",print).short_help(), str)

class AMenu_loop(unittest.IsolatedAsyncioTestCase):
    async def test_aloop(self):
        """Menu.aloop() functions correctly"""
        p1 = inspect.signature(_menu.Menu.__init__).parameters['prompt'].default
        d1 = "Menu Options Here\n"
        d2 = "Second Menu's Options Here\n"
        p2 = p1
        d3 ="Testing async stuff is hard.\n"
        p3 = "You can only go back\n"

        da3 = {}
        da2 = {"three":_menu.AMenu(da3, display=d3, prompt=p3, dwrap=False)}
        da1 = {'two': _menu.AMenu(da2, display=d2, dwrap=False)}
        test_menu = _menu.AMenu(da1, display=d1, dwrap=False)

        test_input = "two\nthree\nsplorp\nback\nback\nback\n"
        spoof_output = io.StringIO()
        spoof_input = io.StringIO(test_input)

        expected_output = f"{d1}{p1}{d2}{p2}{d3}{p3}Invalid command.\n{p3}{d2}{p2}{d1}{p2}"

        menu_coro = test_menu.loop(_in_stream=spoof_input, _out_stream=spoof_output)
        await menu_coro

        spoof_output.seek(0)


        self.assertEqual(expected_output, spoof_output.read())

class Menu_class(unittest.TestCase):
    def setUp(self) -> None:
        self.test_dict = {
            "one":{
                "one":lambda:None,
                "two":_menu.Menu(one=lambda:None, display="5")
            },
            "two":{
                "one":lambda:None,
                "two":lambda:None,
                "three":_menu.AMenu(one=lambda:None, _aprint=print)
            },
        }

    def test_loop(self):
        """Menu.loop() functions correctly"""
        p1 = inspect.signature(_menu.Menu.__init__).parameters['prompt'].default
        d1 = "Menu Options Here\n"
        d2 = "Second Menu's Options Here\n"
        p2 = p1
        d3 ="Testing async stuff is hard.\n"
        p3 = "You can only go back\n"

        da3 = {}
        da2 = {"three":_menu.Menu(da3, display=d3, prompt=p3, dwrap=False)}
        da1 = {'two': _menu.Menu(da2, display=d2, dwrap=False)}
        test_menu = _menu.Menu(da1, display=d1, dwrap=False)

        test_input = "two\nthree\nsplorp\nback\nback\nback\n"
        spoof_output = io.StringIO()
        spoof_input = io.StringIO(test_input)

        expected_output = f"{d1}{p1}{d2}{p2}{d3}{p3}Invalid command.\n{p3}{d2}{p2}{d1}{p2}"

        test_menu.loop(_in_stream=spoof_input, _out_stream=spoof_output)

        spoof_output.seek(0)

        self.assertEqual(expected_output, spoof_output.read())

    def test_init(self):
        """Initialization functions correctly"""
        test = _menu.Menu()
        self.assertNotIn("test", test)
        self.assertEqual(test._display, None)
        self.assertEqual(test._prompt, "Enter command: ")
        self.assertEqual(test.__doc__, "A menu")
        self.assertEqual(len(test), 3)
        self.assertEqual(test.DISPLAY_WRAP, True)
        test = _menu.Menu({"test":lambda:5})
        self.assertEqual(test["test"](), 5)
        test = _menu.Menu(display="5")
        self.assertEqual(test._display,"5")
        test = _menu.Menu(prompt="6")
        self.assertEqual(test._prompt, "6")
        test = _menu.Menu(docs="7")
        self.assertEqual(test.__doc__, "7")
        test = _menu.Menu(suppress_implied=True)
        self.assertEqual(len(test), 0)
        test = _menu.Menu(dwrap=False)
        self.assertEqual(test.DISPLAY_WRAP, False)
        test = _menu.Menu(_menu.Menu({"test":lambda:5}, display="5", prompt="6", docs="7", suppress_implied=True, dwrap=False))
        self.assertEqual(test["test"](), 5)
        self.assertEqual(test._display, None)
        self.assertEqual(test._prompt, "Enter command: ")
        self.assertEqual(test.__doc__, "A menu")
        self.assertEqual(test.DISPLAY_WRAP, True)
        self.assertEqual(len(test), 4)
        test = _menu.Menu({"sub":_menu.Menu({"test":lambda:5}, display="5", prompt="6", docs="7", suppress_implied=True, dwrap=False)})
        self.assertEqual(test["sub"]["test"](), 5)
        self.assertEqual(test["sub"]._display, "5")
        self.assertEqual(test["sub"]._prompt, "6")
        self.assertEqual(test["sub"].__doc__, "7")
        self.assertEqual(len(test["sub"]), 1)
        self.assertEqual(test["sub"].DISPLAY_WRAP, False)

        self.assertRaises(TypeError, _menu.Menu, {"test":{"print":25}})
        self.assertRaises(TypeError, _menu.Menu, {"test":{25:print}})

    def test_validate_dict(self):
        """Menu._validate_dict raises various errors appropriately"""
        test_dict_one = self.test_dict
        test_dict_two = {
            "two":2,
            "four":{
                "print":lambda:"Print 4",
            },
        }

        test_dict_three = {
            "two":{
                "print":lambda:"Print!",
            },
            "four":{
                25:lambda:"Twenty-five"
            }
        }

        self.assertEqual(_menu.Menu._validate_dict(test_dict_one), test_dict_one)
        self.assertRaises(TypeError, _menu.Menu._validate_dict, test_dict_two)
        self.assertRaises(TypeError, _menu.Menu._validate_dict, test_dict_three)

    def test_as_dict(self):
        """menu._as_dict() returns a the Menu's dict, sans implied commands"""
        test = _menu.Menu(self.test_dict)
        test2 = test._as_dict()

        self.assertIsInstance(test, _menu.Menu)
        self.assertIsInstance(test2, dict)
        self.assertIn("back", test)
        self.assertIn("help", test)
        self.assertIn("structure", test)
        self.assertNotIn("back", test2)
        self.assertNotIn("help", test2)
        self.assertNotIn("structure", test2)
        self.assertEqual(test.structure(), _menu.Menu(test2).structure())

    def test_structure(self):
        """menu.structure() returns a sensible output"""
        test_menu = _menu.Menu(self.test_dict)

        test = test_menu.structure(pretty=False)
        expected = {"menu":["one", "two"],
                           "one":{
                               "menu":["one", "two"],
                               "two":{"menu":["one"]}},
                           "two":{
                               "menu":["one", "two", "three"],
                               "three":{"menu":["one"]}}}
        self.assertEqual(test, expected)

        test = test_menu.structure(pretty=True)
        expected = r"""root
    - two
        - three
            - one
        - two
        - one
    - one
        - two
            - one
        - one
"""
        self.assertEqual(test, expected)